import type { NextPage } from 'next';
import Day from '@/days/03';

const Page: NextPage = () => {
  return <Day />;
};

export default Page;
