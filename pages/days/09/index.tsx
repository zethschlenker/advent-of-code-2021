import type { NextPage } from 'next';
import Day from '@/days/09';

const Page: NextPage = () => {
  return <Day />;
};

export default Page;
