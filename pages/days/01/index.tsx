import type { NextPage } from 'next';
import Day from '@/days/01';

const Page: NextPage = () => {
  return <Day />;
};

export default Page;
