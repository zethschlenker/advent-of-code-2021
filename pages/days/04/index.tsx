import type { NextPage } from 'next';
import Day from '@/days/04';

const Page: NextPage = () => {
  return <Day />;
};

export default Page;
