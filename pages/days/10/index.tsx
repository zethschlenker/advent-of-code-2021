import type { NextPage } from 'next';
import Day from '@/days/10';

const Page: NextPage = () => {
  return <Day />;
};

export default Page;
