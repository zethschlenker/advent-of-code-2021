import type { GetServerSideProps, NextPage } from 'next';
import Link from 'next/link';
import styles from '../styles/Home.module.css';
import getSitemap from '@/util/getSitemap';

interface IPageProps {
  pages: string[];
}

const Home: NextPage<IPageProps> = ({ pages }) => {
  return (
    <div className={styles.home}>
      <ul className={styles.pageList}>
        {pages.map((page) => (
          <li key={page}>
            <Link href={page}>
              <a>{page}</a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Home;

export const getServerSideProps: GetServerSideProps = async () => {
  const staticPages = getSitemap('pages');

  return {
    props: {
      pages: staticPages,
    },
  };
};
