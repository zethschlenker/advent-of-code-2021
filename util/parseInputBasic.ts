const parseInputBasic = (input: string): string[] => {
  return input.trim().split(/\r?\n/);
};

export default parseInputBasic;