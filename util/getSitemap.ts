import fs from 'fs';
import path from 'path';

const getSitemap = (dirPath: string, arrayOfFiles: string[] = []): string[] => {
  const files = fs.readdirSync(dirPath);

  files.forEach(function (file) {
    if (fs.statSync(dirPath + '/' + file).isDirectory()) {
      arrayOfFiles = getSitemap(dirPath + '/' + file, arrayOfFiles);
    } else {
      if (!['_app.tsx', '_document.tsx'].includes(file)) {
        const filePath = path.join(dirPath, '/').replaceAll('pages', '');
        if (filePath !== '/') {
          arrayOfFiles.push(filePath);
        }
      }
    }
  });

  return arrayOfFiles.sort((a: string, b: string) => a.localeCompare(b));

  // return [];
};

export default getSitemap;
