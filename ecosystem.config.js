module.exports = {
  apps: [
    {
      name: 'advent-of-code-2021.zeth.dev',
      script: 'npm',
      args: 'start',
    },
  ],
};
