import { liveLife, parseAges } from '@/days/06/helpers';

interface IPart1 {
  input: string;
  sampleInput: string;
}
const Part1: React.FC<IPart1> = ({ input, sampleInput }) => {
  const sampleInputParsed = parseAges(sampleInput);
  const inputParsed = parseAges(input);

  const sampleResult = liveLife(sampleInputParsed, 18);
  const result = liveLife(inputParsed, 80);

  return (
    <>
      <p>Part 1 (sample): {sampleResult}</p>
      <p>Part 1 (real): {result}</p>
    </>
  );
};

export default Part1;
