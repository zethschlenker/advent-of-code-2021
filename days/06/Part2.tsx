import { liveLifeCautiously, parseAges } from '@/days/06/helpers';

interface IPart2 {
  input: string;
  sampleInput: string;
}
const Part2: React.FC<IPart2> = ({ input, sampleInput }) => {
  const sampleInputParsed = parseAges(sampleInput);
  const inputParsed = parseAges(input);

  const sampleResult = liveLifeCautiously(sampleInputParsed, 256);
  const result = liveLifeCautiously(inputParsed, 256);

  return (
    <>
      <p>Part 2 (sample): {sampleResult}</p>
      <p>Part 2 (real): {result}</p>
    </>
  );
};

export default Part2;
