export const parseAges = (input: string): number[] => {
  return input.split(',').map(Number);
};

export const liveLife = (fishes: number[], daysLeft: number): number => {
  // Base case: Just count the fish.
  if (daysLeft === 0) {
    return fishes.length;
  }

  // Save how many new fish were created today.
  let newFishes = 0;

  // Otherwise, update each fish and go to the next day.
  const changedFishes = fishes.map((fish) => {
    // 0s become 6s.
    if (fish === 0) {
      newFishes++;
      return 6;
    }

    // Decrement fish age.
    return fish - 1;
  });

  // Recursion! Add new fishes, reduce day, move on!
  return liveLife(
    [...changedFishes, ...Array.from({ length: newFishes }, () => 8)],
    daysLeft - 1,
  );
};

export const liveLifeCautiously = (
  fishes: number[],
  daysLeft: number,
): number => {
  // Track count of fish at each index.
  const tracker = Array.from({ length: 9 }, () => 0);

  // Initialize tracker.
  fishes.forEach((fish) => {
    tracker[fish]++;
  });

  // TODO: There's a better way to reassign values. I'm just lazy.
  while (daysLeft > 0) {
    const zeroes = tracker[0];

    // 1s are now 0s.
    tracker[0] = tracker[1];
    tracker[1] = 0;

    // 2s are now 1s.
    tracker[1] = tracker[2];
    tracker[2] = 0;

    // 3s are now 2s.
    tracker[2] = tracker[3];
    tracker[3] = 0;

    // 4s are now 3s.
    tracker[3] = tracker[4];
    tracker[4] = 0;

    // 5s are now 4s.
    tracker[4] = tracker[5];
    tracker[5] = 0;

    // 6s are now 5s.
    tracker[5] = tracker[6];
    tracker[6] = zeroes;

    // 7s are now 6s.
    tracker[6] += tracker[7];
    tracker[7] = 0;

    // 8s are now 7s.
    tracker[7] = tracker[8];
    tracker[8] = zeroes;

    // Day is over!
    daysLeft--;
  }

  return tracker.reduce((totalFish, fishCount) => totalFish + fishCount, 0);
};
