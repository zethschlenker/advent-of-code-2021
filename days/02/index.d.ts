interface IDepthPosition {
  /**
   * Wow, a depth.
   */
  depth: number

  /**
   * Wow, a position!
   */
  position: number
}
interface IDepthPositionAim extends IDepthPosition {
  aim: number
}
export interface ICalculateDepthPosition {
  (items: string[]): IDepthPosition
}
export interface ICalculateDepthPositionAim {
  (items: string[]): IDepthPositionAim
}