import type {
  ICalculateDepthPosition,
  ICalculateDepthPositionAim,
} from './index.d';

import input from './input.txt';
import parseInputBasic from '@/util/parseInputBasic';

const parsedInput = parseInputBasic(input);
const calculateDepthAndPosition: ICalculateDepthPosition = (items) => {
  let depth = 0;
  let position = 0;

  items.forEach((item) => {
    const [instruction, rawValue] = item.split(' ');
    const value = parseInt(rawValue, 10);
    switch (instruction) {
      case 'forward':
        position += value;
        break;

      case 'down':
        depth += value;
        break;

      case 'up':
        depth -= value;
        break;
    }
  });

  return {
    depth,
    position,
  };
};

const calculateDepthAndPositionWithAim: ICalculateDepthPositionAim = (
  items,
) => {
  let depth = 0;
  let position = 0;
  let aim = 0;

  items.forEach((item) => {
    const [instruction, rawValue] = item.split(' ');
    const value = parseInt(rawValue, 10);
    switch (instruction) {
      case 'forward':
        position += value;
        depth += value * aim;
        break;

      case 'down':
        aim += value;
        break;

      case 'up':
        aim -= value;
        break;
    }
  });

  return {
    depth,
    position,
    aim,
  };
};

const part1Data = calculateDepthAndPosition(parsedInput);
const part1Result = part1Data.depth * part1Data.position;

const part2Data = calculateDepthAndPositionWithAim(parsedInput);
const part2Result = part2Data.depth * part2Data.position;

const Day: React.FC = () => {
  return (
    <>
      <h2>Day 02</h2>
      <p>Part 1: {part1Result}</p>
      <p>Part 2: {part2Result}</p>
    </>
  );
};

export default Day;
