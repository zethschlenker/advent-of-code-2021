import { findOptimalDistance, parseCrabPositions } from '@/days/07/helpers';

interface IPart1 {
  input: string;
  sampleInput: string;
}
const Part1: React.FC<IPart1> = ({ input, sampleInput }) => {
  const sampleInputParsed = parseCrabPositions(sampleInput);
  const inputParsed = parseCrabPositions(input);
  const sampleResult = findOptimalDistance(sampleInputParsed);
  const result = findOptimalDistance(inputParsed);

  return (
    <>
      <p>Part 1 (sample): {sampleResult}</p>
      <p>Part 1 (real): {result}</p>
    </>
  );
};

export default Part1;
