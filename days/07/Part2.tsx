import { findOptimalDistance, parseCrabPositions } from '@/days/07/helpers';

interface IPart2 {
  input: string;
  sampleInput: string;
}
const Part2: React.FC<IPart2> = ({ input, sampleInput }) => {
  const sampleInputParsed = parseCrabPositions(sampleInput);
  const inputParsed = parseCrabPositions(input);
  const sampleResult = findOptimalDistance(sampleInputParsed, true);
  const result = findOptimalDistance(inputParsed, true);

  return (
    <>
      <p>Part 2(sample): {sampleResult}</p>
      <p>Part 2 (real): {result}</p>
    </>
  );
};

export default Part2;
