export const parseCrabPositions = (input: string): number[] => {
  return input.split(',').map(Number);
};

export const findOptimalDistance = (
  positions: number[],
  summation = false,
): number => {
  const optimal = {
    position: 0,
    fuel: Number.MAX_SAFE_INTEGER,
  };

  // Max position from input. Can't only consider positions from the set.
  const maxPosition = Math.max(...positions);

  // Iterate through all candidate positions in the range of "valid conditions" determined by input.
  for (let i = 0; i < maxPosition; i++) {
    // Add all fuel consumptions for all potential position changes.
    const fuelConsumption = positions.reduce((totalFuel, position) => {
      // Most basic, fuel consumption is just the current position minus candidate position.
      let fuelCost = Math.abs(position - i);

      // Each position move is a summation of the values it took to move to the spot.
      if (summation && fuelCost > 0) {
        fuelCost = (Math.pow(fuelCost, 2) + fuelCost) / 2;
      }

      return totalFuel + fuelCost;
    }, 0);

    // Found a new optimal candidate position!
    if (fuelConsumption < optimal.fuel) {
      optimal.position = i;
      optimal.fuel = fuelConsumption;
    }
  }

  return optimal.fuel;
};
