import { Octopuses } from '@/days/11/helpers';

interface IPart {
  input: string;
  sampleInput: string;
}
const Part: React.FC<IPart> = ({ input, sampleInput }) => {
  const sampleOctopuses = new Octopuses(sampleInput);
  const octopuses = new Octopuses(input);
  const sampleResult = sampleOctopuses.getSynchronizedFlashStep();
  const result = octopuses.getSynchronizedFlashStep();

  return (
    <>
      <p>Part 1 (sample): {sampleResult}</p>
      <p>Part 1 (real): {result}</p>
    </>
  );
};

export default Part;
