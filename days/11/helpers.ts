export class Octopuses {
  private octopusesState: number[][] = [];

  constructor(input: string) {
    this.octopusesState = this.parseInput(input);
  }
  private parseInput(input: string): number[][] {
    return input.split(/\n/).map((line) => line.split('').map(Number));
  }

  public getFlashesAfterSteps(steps = 1): number {
    let flashedOctopuses = 0;

    for (let i = 0; i < steps; i++) {
      const stepOutput = this.step();
      flashedOctopuses += stepOutput;
    }

    return flashedOctopuses;
  }

  public getSynchronizedFlashStep(maxSteps = 1000): number {
    let syncrhonizedFlashStep = -1;

    for (let i = 1; i < maxSteps; i++) {
      const stepOutput = this.step();
      if (stepOutput === this.octopusesState.length ** 2) {
        syncrhonizedFlashStep = i;
        break;
      }
    }

    return syncrhonizedFlashStep;
  }

  private step(): number {
    // Increment.
    this.octopusesState.forEach((octopusesLine, row) => {
      octopusesLine.forEach((octopus, column) => {
        this.incrementOctopus(row, column);
      });
    });

    // Flash.
    const steppedFlashedOctopuses: Set<string> = new Set();
    this.octopusesState.forEach((octopusesLine, row) => {
      octopusesLine.forEach((octopus, column) => {
        this.flashOctopus(row, column, steppedFlashedOctopuses);
      });
    });

    // Reset.
    this.octopusesState.forEach((octopusesLine, row) => {
      octopusesLine.forEach((octopus, column) => {
        this.resetOctopus(row, column, steppedFlashedOctopuses);
      });
    });

    return steppedFlashedOctopuses.size;
  }

  private incrementOctopus(row: number, column: number): void {
    this.octopusesState[row][column]++;
  }

  private flashOctopus(
    row: number,
    column: number,
    flashedOctopuses: Set<string>,
  ): void {
    const coordinateString = JSON.stringify([row, column]);
    if (
      this.octopusesState[row][column] > 9 &&
      !flashedOctopuses.has(coordinateString)
    ) {
      flashedOctopuses.add(coordinateString);
      const surroundingCoordinates = this.getSurroundingCoordinates(
        row,
        column,
      );

      surroundingCoordinates.forEach((surroundingCoordinate) => {
        this.incrementOctopus(
          surroundingCoordinate[0],
          surroundingCoordinate[1],
        );
      });
      surroundingCoordinates.forEach((surroundingCoordinate) => {
        this.flashOctopus(
          surroundingCoordinate[0],
          surroundingCoordinate[1],
          flashedOctopuses,
        );
      });
    }
  }

  private resetOctopus(
    row: number,
    column: number,
    flashedOctopuses: Set<string>,
  ): void {
    const coordinateString = JSON.stringify([row, column]);
    if (flashedOctopuses.has(coordinateString)) {
      this.octopusesState[row][column] = 0;
    }
  }

  private getSurroundingCoordinates(row: number, column: number): number[][] {
    return [
      // North.
      [row - 1, column],
      // East.
      [row, column + 1],
      // South.
      [row + 1, column],
      // West.
      [row, column - 1],
      // Northeast
      [row - 1, column + 1],
      // Southeast.
      [row + 1, column + 1],
      // Southwest.
      [row + 1, column - 1],
      // Northwest.
      [row - 1, column - 1],
    ].filter((coordinate) =>
      this.checkValidCoordinate(coordinate[0], coordinate[1]),
    );
  }
  private checkValidCoordinate(row: number, column: number): boolean {
    return (
      row >= 0 &&
      row < this.octopusesState.length &&
      column >= 0 &&
      column < this.octopusesState[0].length
    );
  }
}
