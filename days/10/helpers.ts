export const parseSyntax = (input: string): string[][] => {
  return input.split(/\n/).map((row) => row.split(''));
};

interface CategorizedLine {
  type: 'corrupted' | 'incomplete';
  line: string[];
}
interface CorruptedLine extends CategorizedLine {
  type: 'corrupted';
  corruptedCharacter: string;
  row: number;
  column: number;
}
interface IncompleteLine extends CategorizedLine {
  type: 'incomplete';
  closingCharacters: string[];
}
const getClosingCharacter = (openingCharacter: string): string => {
  const characterMap: { [key: string]: string } = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>',
  };

  if (openingCharacter in characterMap) {
    return characterMap[openingCharacter];
  }
  return '';
};
export const categorizeLines = (
  lines: string[][],
): (CorruptedLine | IncompleteLine)[] => {
  const maxLength = Math.max(...lines.map((line) => line.length));

  const categorizedLines: (CorruptedLine | IncompleteLine)[] = [];

  lines.forEach((line, row) => {
    const openingChars: string[] = [];
    let lineCorrupted = false;
    line.forEach((character, column) => {
      // If already corrupted, stop looking.
      if (lineCorrupted) {
        return;
      }

      // First element, just log the character and go to next element.
      if (column === 0) {
        openingChars.push(character);
        return;
      }

      const correspondingCharacter = getClosingCharacter(character);
      const lastCharacter = openingChars.slice(-1)[0];
      const lastCorrespondingCharacter = getClosingCharacter(lastCharacter);

      // If it's another opening characer, log it and move on.
      if (correspondingCharacter.length) {
        openingChars.push(character);
      } else {
        // It's a closing character. Check if it matches what we expected.
        if (character !== lastCorrespondingCharacter && column < maxLength) {
          lineCorrupted = true;
          categorizedLines.push({
            type: 'corrupted',
            line,
            row,
            column,
            corruptedCharacter: character,
          });
        } else if (character === lastCorrespondingCharacter) {
          // We found a good closing charcter. No need to keep tracking it.
          openingChars.pop();
        }
      }
    });

    // If we made it this far, then the line isn't corrupted.
    if (!lineCorrupted) {
      categorizedLines.push({
        type: 'incomplete',
        line,
        closingCharacters: openingChars
          .reverse()
          .map((character) => getClosingCharacter(character)),
      });
    }
  });

  return categorizedLines;
};

export const scoreCorruptedLines = (
  categorizedLines: (CorruptedLine | IncompleteLine)[],
): number => {
  const characterScoreMap: { [key: string]: number } = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137,
  };

  return (
    categorizedLines
      // Only consider corrupted lines.
      .filter(
        (categorizedLine): categorizedLine is CorruptedLine =>
          categorizedLine.type === 'corrupted',
      )
      // Follow the formula (just addition)
      .reduce(
        (total, categorizedLine) =>
          total + characterScoreMap[categorizedLine.corruptedCharacter],
        0,
      )
  );
};

export const scoreIncompleteLines = (
  categorizedLines: (CorruptedLine | IncompleteLine)[],
): number => {
  const characterScoreMap: { [key: string]: number } = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4,
  };

  const lineScores = categorizedLines
    // Only consider incomplete items.
    .filter(
      (categorizedLine): categorizedLine is IncompleteLine =>
        categorizedLine.type === 'incomplete',
    )
    // Follow the formula, for each character => ((5 * currentTotal) + characterScore)
    .map((incompleteLine) => {
      return incompleteLine.closingCharacters.reduce(
        (characterTotal, character) =>
          5 * characterTotal + characterScoreMap[character],
        0,
      );
    });

  return lineScores.sort((a, b) => a - b)[Math.floor(lineScores.length / 2)];
};
