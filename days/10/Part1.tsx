import {
  categorizeLines,
  parseSyntax,
  scoreCorruptedLines,
} from '@/days/10/helpers';

interface IPart {
  input: string;
  sampleInput: string;
}
const Part: React.FC<IPart> = ({ input, sampleInput }) => {
  const sampleInputParsed = parseSyntax(sampleInput);
  const inputParsed = parseSyntax(input);
  const sampleResult = scoreCorruptedLines(categorizeLines(sampleInputParsed));
  const result = scoreCorruptedLines(categorizeLines(inputParsed));

  return (
    <>
      <p>Part 1 (sample): {sampleResult}</p>
      <p>Part 1 (real): {result}</p>
    </>
  );
};

export default Part;
