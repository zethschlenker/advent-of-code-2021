import input from './input.txt';
import parseInputBasic from '@/util/parseInputBasic';
import { calculateGammaEpsilon, calculateOxygenCo2 } from './helpers';
const parsedInput = parseInputBasic(input);

const part1Data = calculateGammaEpsilon(parsedInput);
const part1Result = part1Data.gamma * part1Data.epsilon;

const part2Data = calculateOxygenCo2(parsedInput);
const part2Result = part2Data.oxygen * part2Data.co2;

const Day: React.FC = () => {
  return (
    <>
      <h2>Day 03</h2>
      <p>Part 1: {part1Result}</p>
      <p>Part 2: {part2Result}</p>
    </>
  );
};

export default Day;
