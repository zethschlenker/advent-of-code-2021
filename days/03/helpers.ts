interface IReduceItems {
  (
    items: string[],
    /**
     * @default true
     */
    matchDominant?: boolean
  ): string[]
}
const reduceItems: IReduceItems = (items, matchDominant = true) => {
  let i = 0;
  return items.reduce((prevItems) => {
    if (prevItems.length === 1) {
      return prevItems;
    }

    const onesCount = prevItems.reduce((previousCount, item) => {
      return (item[i] === '1') ? previousCount + 1 : previousCount;
    }, 0);

    const dominantBit = onesCount >= prevItems.length / 2 ? '1' : '0';

    const checkI = i;
    i++;
    return prevItems.filter((item) => {
      if (matchDominant) {
        return item[checkI] === dominantBit;
      } else {
        return item[checkI] !== dominantBit;
      }
    });
  }, items);
};

interface ICalculateGammaEpsilon {
  (
    items: string[],
  ): {
    gamma: number
    epsilon: number
  }
}
export const calculateGammaEpsilon: ICalculateGammaEpsilon = (items) => {
  let gamma = 0;
  let epsilon = 0;

  const iterations = items[0].length;
  let commonBits = '';
  let commonBitsInverse = '';

  for (let i = 0; i < iterations; i++) {
    const onesCount = items.reduce((previousValue, item) => {
      return (item[i] === '1') ? previousValue + 1 : previousValue;
    }, 0);

    if (onesCount >= items.length / 2) {
      commonBits += '1';
      commonBitsInverse += '0';
    } else {
      commonBits += '0';
      commonBitsInverse += '1';
    }
  }

  gamma = parseInt(commonBits, 2);
  epsilon = parseInt(commonBitsInverse, 2);

  return {
    gamma,
    epsilon,
  };
};

interface ICalculateOxygenCo2 {
  (
    items: string[],
  ): {
    oxygen: number
    co2: number
  }
}
export const calculateOxygenCo2: ICalculateOxygenCo2 = (items) => {
  let oxygen = 0;
  let co2 = 0;

  const oxygenItems = reduceItems(items);
  const co2Items = reduceItems(items, false);

  oxygen = parseInt(oxygenItems[0], 2);
  co2 = parseInt(co2Items[0], 2);

  return {
    oxygen,
    co2,
  };
};