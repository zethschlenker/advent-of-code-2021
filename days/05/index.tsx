import input from './input.txt';
import { countIntersections, parseCoordinates } from '@/days/05/helpers';
const parsedInput = parseCoordinates(input);

const part1Data = countIntersections(parsedInput);
const part1Result = part1Data;
const part2Data = countIntersections(parsedInput, true);
const part2Result = part2Data;

const Day: React.FC = () => {
  return (
    <>
      <h2>Day 05</h2>
      <p>Part 1: {part1Result}</p>
      <p>Part 2: {part2Result}</p>
    </>
  );
};

export default Day;
