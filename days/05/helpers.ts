interface Coordinate {
  x: number;
  y: number;
}
interface CoordinatePair {
  direction: 'horizontal' | 'vertical' | 'diagonal';
  start: Coordinate;
  end: Coordinate;
  slope: number;
  yIntercept: number;
}
export const parseCoordinates = (input: string): CoordinatePair[] => {
  // Split by line.
  const coordinatesPairs: CoordinatePair[] = input
    .trim()
    .split(/\n/g)
    .map((rawCoordinates) => {
      // Get the numeric coordinates from each line.
      const [coordA, coordB] = rawCoordinates
        .split(' -> ')
        .map((rawCoords) => rawCoords.split(',').map(Number));

      // Determine the direction based on matching values between coordinates.
      const direction =
        coordA[0] === coordB[0]
          ? 'vertical'
          : coordA[1] === coordB[1]
          ? 'horizontal'
          : 'diagonal';

      // Calculate slope and y-intercept of line.
      const slope = (coordB[1] - coordA[1]) / (coordB[0] - coordA[0]);
      const yIntercept = coordA[1] - slope * coordA[0];

      return {
        direction,
        slope,
        yIntercept,
        start: {
          x: coordA[0],
          y: coordA[1],
        },
        end: {
          x: coordB[0],
          y: coordB[1],
        },
      };
    });

  return coordinatesPairs;
};

const calculateDimensions = (
  coordinatePairs: CoordinatePair[],
): { width: number; height: number } => {
  let width = 1;
  let height = 1;

  // Determine the dimensions based on the max value of the coordinates.
  coordinatePairs.forEach((coordinatePair) => {
    width = Math.max(
      width,
      coordinatePair.start.x + 1,
      coordinatePair.end.x + 1,
    );
    height = Math.max(
      height,
      coordinatePair.start.y + 1,
      coordinatePair.end.y + 1,
    );
  });

  return {
    width,
    height,
  };
};
export const countIntersections = (
  coordinatesPairs: CoordinatePair[],
  allowDiagonal = false,
  minimumIntersections = 2,
): number => {
  const { width, height } = calculateDimensions(coordinatesPairs);

  // Intialize the board for marking intersections.
  const intersectionBoard: number[][] = Array.from({ length: height }, () =>
    Array.from({ length: width }, () => 0),
  );

  // Mark points that pass through coordinate pairs.
  coordinatesPairs.forEach((coordinatePair) => {
    // Depending on the line direction, calculate where the line passes through.
    const startX = Math.min(coordinatePair.start.x, coordinatePair.end.x);
    const endX = Math.max(coordinatePair.start.x, coordinatePair.end.x);
    const startY = Math.min(coordinatePair.start.y, coordinatePair.end.y);
    const endY = Math.max(coordinatePair.start.y, coordinatePair.end.y);

    switch (coordinatePair.direction) {
      case 'horizontal':
        const y = coordinatePair.start.y;
        for (let x = startX; x <= endX; x++) {
          intersectionBoard[y][x]++;
        }
        break;

      case 'vertical':
        const x = coordinatePair.start.x;
        for (let y = startY; y <= endY; y++) {
          intersectionBoard[y][x]++;
        }
        break;

      case 'diagonal':
        if (!allowDiagonal) {
          break;
        }

        // Iterate all points within "zone" of coordinates.
        for (let y = startY; y <= endY; y++) {
          for (let x = startX; x <= endX; x++) {
            // If the point matches the line equation of y = mx + b, mark it.
            if (y === coordinatePair.slope * x + coordinatePair.yIntercept) {
              intersectionBoard[y][x]++;
            }
          }
        }

        break;
    }
  });

  // Count intersections that meet the minimum requirements.
  const intersectionCount = intersectionBoard
    .flat()
    .reduce(
      (prev, curr) => (curr >= minimumIntersections ? prev + 1 : prev),
      0,
    );

  return intersectionCount;
};
