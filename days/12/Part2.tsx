import { Paths } from '@/days/12/helpers';

interface IPart {
  input: string;
  sampleInput: string;
}
const Part: React.FC<IPart> = ({ input, sampleInput }) => {
  const samplePaths = new Paths(sampleInput, true);
  const sampleResult = samplePaths.getTotalPaths();

  const paths = new Paths(input, true);
  const result = paths.getTotalPaths();

  return (
    <>
      <p>Part 2 (sample): {sampleResult}</p>
      <p>Part 2 (real): {result}</p>
    </>
  );
};

export default Part;
