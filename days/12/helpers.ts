interface Cave {
  name: string;
  size: 'big' | 'small';
  start: boolean;
  end: boolean;
  siblings: Set<string>;
}

export class Paths {
  private caves: Cave[] = [];
  private visitSmallTwice = false;

  constructor(input: string, visitSmallTwice = false) {
    this.caves = this.parseInput(input);
    this.visitSmallTwice = visitSmallTwice;
  }
  private parseInput(input: string): Cave[] {
    const foundCaves: Cave[] = [];
    input.split(/\n/).forEach((line) => {
      const [a, b] = line.split('-');

      // Cave A.
      const foundA = foundCaves.findIndex((cave) => cave.name === a);
      if (foundA > -1) {
        foundCaves[foundA].siblings.add(b);
      } else {
        foundCaves.push({
          name: a,
          size: a.toUpperCase() === a ? 'big' : 'small',
          start: a === 'start',
          end: a === 'end',
          siblings: new Set<string>().add(b),
        });
      }

      // Cave B.
      const foundB = foundCaves.findIndex((cave) => cave.name === b);
      if (foundB > -1) {
        foundCaves[foundB].siblings.add(a);
      } else {
        foundCaves.push({
          name: b,
          size: b.toUpperCase() === b ? 'big' : 'small',
          start: b === 'start',
          end: b === 'end',
          siblings: new Set<string>().add(a),
        });
      }
    });

    return foundCaves;
  }

  public getTotalPaths(): number {
    const startingCave = this.caves.find((cave) => cave.start);
    if (typeof startingCave === 'undefined') {
      throw new Error('No starting cave found!');
    }

    const foundPaths: Cave[][] = [];
    if (this.visitSmallTwice) {
      this.traverseWithDuplicates(startingCave, [], foundPaths);
    } else {
      this.traverse(startingCave, [], foundPaths);
    }

    return foundPaths.length;
  }

  private traverse(
    cave: Cave,
    currentPath: Cave[],
    foundPaths: Cave[][],
  ): void {
    const nextPath = [...currentPath, cave];
    // We hit the end!
    if (cave.end) {
      foundPaths.push(nextPath);
      return;
    }

    // Keep going.
    cave.siblings.forEach((sibling) => {
      const foundSibling = this.caves.find(
        (possible) => possible.name === sibling,
      );
      if (typeof foundSibling !== 'undefined') {
        // If it's a big cave or an unvisited small cave, keep going.
        if (
          foundSibling.size === 'big' ||
          currentPath.findIndex(
            (possible) => possible.name === foundSibling.name,
          ) === -1
        ) {
          this.traverse(foundSibling, nextPath, foundPaths);
        }
      }
    });
  }

  private traverseWithDuplicates(
    cave: Cave,
    currentPath: Cave[],
    foundPaths: Cave[][],
  ): void {
    const nextPath = [...currentPath, cave];

    // If we're at the start, but have traversed already, don't come back to the start!
    if (cave.start && currentPath.length > 0) {
      return;
    }

    // We hit the end!
    if (cave.end) {
      foundPaths.push(nextPath);
      return;
    }

    // Mark visited small caves in current path.
    const caveCounts: { [key: string]: number } = {};
    nextPath.forEach((nextCave) => {
      if (nextCave.size === 'small') {
        caveCounts[nextCave.name] = (caveCounts[nextCave.name] ?? 0) + 1;
      }
    });

    // Are any items in the proposed next path duplicates?
    const pathHasSmallDuplicates = Object.values(caveCounts).some(
      (caveCount) => caveCount > 1,
    );

    // Keep going.
    cave.siblings.forEach((sibling) => {
      const foundSibling = this.caves.find(
        (possible) => possible.name === sibling,
      );
      if (typeof foundSibling !== 'undefined') {
        const notInCurrentPath =
          currentPath.findIndex(
            (possible) => possible.name === foundSibling.name,
          ) === -1;
        if (
          !pathHasSmallDuplicates ||
          foundSibling.size === 'big' ||
          notInCurrentPath
        ) {
          this.traverseWithDuplicates(foundSibling, nextPath, foundPaths);
        }
      }
    });
  }
}
