import { Paths } from '@/days/12/helpers';

interface IPart {
  input: string;
  sampleInput: string;
}
const Part: React.FC<IPart> = ({ input, sampleInput }) => {
  const samplePaths = new Paths(sampleInput);
  const sampleResult = samplePaths.getTotalPaths();

  const paths = new Paths(input);
  const result = paths.getTotalPaths();

  return (
    <>
      <p>Part 1 (sample): {sampleResult}</p>
      <p>Part 1 (real): {result}</p>
    </>
  );
};

export default Part;
