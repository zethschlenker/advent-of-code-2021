import { ReactElement } from 'react';

export const parseHeightMap = (input: string): number[][] => {
  return input.split(/\n/).map((row) => row.split('').map(Number));
};

const valueOrNine = (value: number | undefined): number => {
  if (typeof value === 'undefined') {
    return 9;
  }

  return value;
};

interface LowPoint {
  row: number;
  column: number;
  value: number;
}
export const findLowPoints = (heightMap: number[][]): LowPoint[] => {
  const width = heightMap[0].length;
  const height = heightMap.length;

  const lowPoints: LowPoint[] = [];

  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width; j++) {
      const point = heightMap[i][j];
      if (
        point < valueOrNine(heightMap[i + 1]?.[j]) &&
        point < valueOrNine(heightMap[i - 1]?.[j]) &&
        point < valueOrNine(heightMap[i]?.[j + 1]) &&
        point < valueOrNine(heightMap[i]?.[j - 1])
      ) {
        lowPoints.push({
          row: i,
          column: j,
          value: point,
        });
      }
    }
  }

  return lowPoints;
};

export const calculateRiskLevels = (lowPoints: LowPoint[]): number => {
  return lowPoints.reduce((total, lowPoint) => total + lowPoint.value + 1, 0);
};

export const findBasinPoints = (
  heightMap: number[][],
  lowPoints: LowPoint[],
): number => {
  const width = heightMap[0].length;
  const height = heightMap.length;

  // Track basins we've already been to.
  const visitedBasins: Set<string> = new Set();

  const followBasin = (
    basin: Set<string>,
    row: number,
    column: number,
  ): void => {
    // Stop looking if the point is out of bounds.
    if (
      row < 0 ||
      column < 0 ||
      row >= height ||
      column >= width ||
      visitedBasins.has(JSON.stringify([row, column]))
    ) {
      return;
    }

    // Okay, we've visited this point.
    visitedBasins.add(JSON.stringify([row, column]));

    // Stop looking if this point is the max value.
    const value = heightMap[row][column];
    if (value === 9) {
      return;
    }

    // Good basin, add it to the basin for the low point.
    basin.add(JSON.stringify([row, column]));

    // Follow points connected to the current point.

    // West.
    followBasin(basin, row - 1, column);

    // East.
    followBasin(basin, row + 1, column);

    // North.
    followBasin(basin, row, column - 1);

    // South.
    followBasin(basin, row, column + 1);
  };

  const basins: Set<string>[] = [];
  lowPoints.forEach((lowPoint) => {
    const basin = new Set<string>();
    followBasin(basin, lowPoint.row, lowPoint.column);
    basins.push(basin);
  });

  // Find the three largest basins based on their lengths.
  const [firstBasin, secondBasin, thirdBasin] = basins.sort(
    (basinA, basinB) => {
      return basinB.size - basinA.size;
    },
  );

  return [firstBasin, secondBasin, thirdBasin].reduce(
    (total, basin) => total * basin.size,
    1,
  );
};
