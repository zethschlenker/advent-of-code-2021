import input from './input.txt';
import sampleInput from './sampleInput.txt';

import Part1 from './Part1';
import Part2 from './Part2';

const Day: React.FC = () => {
  return (
    <>
      <h2>Day 09</h2>
      <Part1 input={input} sampleInput={sampleInput} />
      <Part2 input={input} sampleInput={sampleInput} />
    </>
  );
};

export default Day;
