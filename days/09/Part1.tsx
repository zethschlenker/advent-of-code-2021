import {
  calculateRiskLevels,
  findLowPoints,
  parseHeightMap,
} from '@/days/09/helpers';

interface IPart {
  input: string;
  sampleInput: string;
}
const Part: React.FC<IPart> = ({ input, sampleInput }) => {
  const sampleInputParsed = parseHeightMap(sampleInput);
  const inputParsed = parseHeightMap(input);
  const sampleResult = calculateRiskLevels(findLowPoints(sampleInputParsed));
  const result = calculateRiskLevels(findLowPoints(inputParsed));

  const inputLowPoints = findLowPoints(inputParsed);

  const isLowPoint = (row: number, column: number): boolean => {
    return inputLowPoints.some(
      (lowPoint) => lowPoint.row === row && lowPoint.column === column,
    );
  };

  return (
    <>
      <p>Part 1 (sample): {sampleResult}</p>
      <p>Part 1 (real): {result}</p>
      {inputParsed.map((row, i) => (
        <div key={`row-${i}`} style={{ fontFamily: 'monospace' }}>
          {row.map((column, j) => (
            <span
              key={`col-${j}-$column`}
              style={{
                color: isLowPoint(i, j) ? 'red' : 'black',
              }}
            >
              {column}
            </span>
          ))}
        </div>
      ))}
    </>
  );
};

export default Part;
