import {
  calculateRiskLevels,
  findBasinPoints,
  findLowPoints,
  parseHeightMap,
} from '@/days/09/helpers';

interface IPart {
  input: string;
  sampleInput: string;
}
const Part: React.FC<IPart> = ({ input, sampleInput }) => {
  const sampleInputParsed = parseHeightMap(sampleInput);
  const sampleInputLowPoints = findLowPoints(sampleInputParsed);
  const inputParsed = parseHeightMap(input);
  const inputLowPoints = findLowPoints(inputParsed);
  const sampleResult = findBasinPoints(sampleInputParsed, sampleInputLowPoints);
  const result = findBasinPoints(inputParsed, inputLowPoints);

  return (
    <>
      <p>Part 1 (sample): {sampleResult}</p>
      <p>Part 1 (real): {result}</p>
    </>
  );
};

export default Part;
