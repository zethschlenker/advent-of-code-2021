import { union, intersection, difference } from 'lodash';
interface EncodedSignalPattern {
  signalPatterns: string[];
  digits: string[];
}

export const parseSignalPatterns = (input: string): EncodedSignalPattern[] => {
  return input.split(/\n/).map((line) => {
    const [lineSignalPatterns, lineOutput] = line.split(' | ');
    return {
      signalPatterns: lineSignalPatterns.split(' '),
      digits: lineOutput.split(' '),
    };
  });
};

export const countDigitInstances = (
  encodedSignalPatterns: EncodedSignalPattern[],
): number => {
  const allowedLengths = [2, 3, 4, 7];

  return encodedSignalPatterns.reduce((total, encodedSignalPattern) => {
    return (
      total +
      encodedSignalPattern.digits.filter((digit) => {
        return allowedLengths.includes(digit.length);
      }).length
    );
  }, 0);
};

export const decodeSignalPattern = (signalPattern: string[]): string[] => {
  const mappedValueToLetter: string[] = Array.from({ length: 10 }, () => '');

  // Value    -     Length    -   Status
  // 0              6             done
  // 1              2             done
  // 2              5
  // 3              5
  // 4              4             done
  // 5              5             done
  // 6              6             done
  // 7              3             done
  // 8              7             done
  // 9              6             done

  // Prioritize finding 1, 4, 7, and 8 based on their lengths.
  mappedValueToLetter[1] =
    signalPattern.find((pattern) => pattern.length === 2) || '';
  mappedValueToLetter[4] =
    signalPattern.find((pattern) => pattern.length === 4) || '';
  mappedValueToLetter[7] =
    signalPattern.find((pattern) => pattern.length === 3) || '';
  mappedValueToLetter[8] =
    signalPattern.find((pattern) => pattern.length === 7) || '';

  const fourSevenUnion = new Set(
    union(mappedValueToLetter[4]?.split(''), mappedValueToLetter[7]?.split('')),
  );

  // 9 = 4 union 7 (+ one extra).
  mappedValueToLetter[9] =
    signalPattern.find((pattern) => {
      return (
        pattern.length === 6 &&
        pattern.split('').filter((char) => fourSevenUnion.has(char)).length ===
          5
      );
    }) || '';

  const nineOneDifference = new Set(
    difference(
      mappedValueToLetter[9]?.split(''),
      mappedValueToLetter[1]?.split(''),
    ),
  );

  // 5 = 9 difference 1 (+ one extra)
  mappedValueToLetter[5] =
    signalPattern.find((pattern) => {
      return (
        pattern.length === 5 &&
        pattern.split('').filter((char) => nineOneDifference.has(char))
          .length === 4
      );
    }) || '';

  // 6 = (8 - 5 - 1) + 5.
  const eightFiveOneDifference = difference(
    mappedValueToLetter[8]?.split(''),
    mappedValueToLetter[5]?.split(''),
    mappedValueToLetter[1],
  );
  const eightFiveOneDifferenceSet = new Set(eightFiveOneDifference);
  mappedValueToLetter[6] =
    signalPattern.find((pattern) => {
      return (
        pattern.length === 6 &&
        pattern.split('').filter((char) => eightFiveOneDifferenceSet.has(char))
          .length === 1 &&
        !mappedValueToLetter.includes(pattern)
      );
    }) || '';

  // 0 is the only six-length long.
  mappedValueToLetter[0] =
    signalPattern.find((pattern) => {
      return pattern.length === 6 && !mappedValueToLetter.includes(pattern);
    }) || '';

  // Two items left: 2 or 3
  const remainingItemsTwoThree = signalPattern
    .filter((pattern) => {
      return pattern.length === 5 && !mappedValueToLetter.includes(pattern);
    })
    .map((pattern) => pattern.split('').sort().join(''));

  // 3 + 1 === 3
  const remainingOptionOne = union(
    remainingItemsTwoThree[0]?.split(''),
    mappedValueToLetter[1]?.split(''),
  ).join('');

  if (remainingItemsTwoThree.includes(remainingOptionOne)) {
    mappedValueToLetter[3] = remainingItemsTwoThree[0];
    mappedValueToLetter[2] = remainingItemsTwoThree[1];
  } else {
    mappedValueToLetter[2] = remainingItemsTwoThree[0];
    mappedValueToLetter[3] = remainingItemsTwoThree[1];
  }

  return mappedValueToLetter.map((letter) => letter?.split('').sort().join(''));
};

const calculateDigitValues = (
  mappedValues: string[],
  digits: string[],
): number => {
  const sortedDigits = digits.map((digit) => digit.split('').sort().join(''));

  return parseInt(
    sortedDigits.reduce((total, digit) => {
      return total + mappedValues.indexOf(digit);
    }, ''),
    10,
  );
};

export const playBall = (
  encodedSignalPatterns: EncodedSignalPattern[],
): number => {
  return encodedSignalPatterns.reduce((total, encodedSignalPattern) => {
    const mappedValues = decodeSignalPattern(
      encodedSignalPattern.signalPatterns,
    );

    return (
      total + calculateDigitValues(mappedValues, encodedSignalPattern.digits)
    );
  }, 0);
};
