import { countDigitInstances, parseSignalPatterns } from '@/days/08/helpers';

interface IPart {
  input: string;
  sampleInput: string;
}
const Part: React.FC<IPart> = ({ input, sampleInput }) => {
  const sampleInputParsed = parseSignalPatterns(sampleInput);
  const inputParsed = parseSignalPatterns(input);
  const sampleResult = countDigitInstances(sampleInputParsed);
  const result = countDigitInstances(inputParsed);

  return (
    <>
      <p>Part 1 (sample): {sampleResult}</p>
      <p>Part 1 (real): {result}</p>
    </>
  );
};

export default Part;
