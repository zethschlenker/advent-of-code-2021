import { parseSignalPatterns, playBall } from '@/days/08/helpers';

interface IPart {
  input: string;
  sampleInput: string;
}
const Part: React.FC<IPart> = ({ input, sampleInput }) => {
  const sampleInputParsed = parseSignalPatterns(sampleInput);
  const inputParsed = parseSignalPatterns(input);
  const sampleResult = playBall(sampleInputParsed);
  const result = playBall(inputParsed);
  // const result = '';

  return (
    <>
      <p>Part 2 (sample): {sampleResult}</p>
      <p>Part 2 (real): {result}</p>
    </>
  );
};

export default Part;
