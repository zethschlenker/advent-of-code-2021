import input from './input.txt';
import parseInputBasic from '@/util/parseInputBasic';

const parsedInput = parseInputBasic(input).map(Number);
interface ICountIncreases {
  (items: number[]): number;
}
const countIncreases: ICountIncreases = (items) => {
  let count = 0;
  let prevItem: number | null = null;
  items.forEach((item, i) => {
    if (i > 0 && prevItem !== null && item > prevItem) {
      count++;
    }

    prevItem = item;
  });
  return count;
};

interface IChunkInput {
  (items: number[]): number[];
}
const chunkInput: IChunkInput = (items) => {
  const output: number[] = [];

  items.forEach((item, i) => {
    const next = i + 1 > items.length - 1 ? 0 : items[i + 1];
    const afterNext = i + 2 > items.length - 1 ? 0 : items[i + 2];
    output.push([item, next, afterNext].reduce((prev, curr) => prev + curr));
  });

  return output;
};

const part1Result = countIncreases(parsedInput);
const part2Result = countIncreases(chunkInput(parsedInput));

const Day = () => {
  return (
    <>
      <h2>Day 01</h2>
      <p>Part 1: {part1Result}</p>
      <p>Part 2: {part2Result}</p>
    </>
  );
};

export default Day;
