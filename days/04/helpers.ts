export const parseBoardInput = (
  input: string,
): { numbers: string[]; boards: string[][][] } => {
  const [numbers, ...boards] = input.trim().split(/\n\n/g);

  return {
    numbers: numbers.split(','),
    boards: boards.map((board) =>
      board.split(/\n/g).map((boardRow) => boardRow.trim().split(/\s+/)),
    ),
  };
};

/**
 *
 * @param board multi-dimensional "board"
 * @param called list of things that have already been called
 *
 * @returns "score" of a winning board
 */
export const checkBoardWin = (board: string[][], called: string[]): number => {
  let score = -1;
  const lastCalled = called.slice(-1)[0];
  let win = false;

  const width = 5;
  const height = 5;

  // Intialize the board for marking with false values.
  const markedBoard: boolean[][] = Array.from({ length: 5 }, () =>
    Array.from({ length: 5 }, () => false),
  );

  // Mark the board according to called numbers.
  for (let row = 0; row < width; row++) {
    for (let col = 0; col < height; col++) {
      const isMarked = called.indexOf(board[row][col]) !== -1;
      if (isMarked) {
        markedBoard[row][col] = true;
      }
    }
  }

  // Check for win state.
  for (let row = 0; row < width; row++) {
    // Is the row a winner?.
    const rowWin = board[row].every((rowCol) => called.includes(rowCol));
    if (rowWin) {
      win = true;
      break;
    }

    // Is the column a winner?
    for (let col = 0; col < height; col++) {
      // TODO: Better way to do this, but brain isn't working right now.
      const colWin = [
        board[0][col],
        board[1][col],
        board[2][col],
        board[3][col],
        board[4][col],
      ].every((rowCol) => called.includes(rowCol));

      if (colWin) {
        win = true;
        break;
      }
    }
  }

  // Found a winner, count unmarked cells.
  if (win) {
    let sumUnmarked = 0;
    for (let i = 0; i < width; i++) {
      for (let j = 0; j < height; j++) {
        const markedCell = markedBoard[i][j];
        if (!markedCell) {
          sumUnmarked += parseInt(board[i][j], 10);
        }
      }
    }

    score = parseInt(lastCalled, 10) * sumUnmarked;
  }

  return score;
};

export const playGame = (boards: string[][][], calling: string[]): number => {
  let winningScore = -1;

  // Call the numbers!
  iterateCalling: for (let i = 0; i < calling.length; i++) {
    // Ask each board if they won.
    for (let j = 0; j < boards.length; j++) {
      const board = boards[j];
      const boardScore = checkBoardWin(board, calling.slice(0, i + 1));

      // If the board won, stop playing.
      if (boardScore > 0) {
        winningScore = boardScore;
        break iterateCalling;
      }
    }
  }

  return winningScore;
};

export const playLosingGame = (
  boards: string[][][],
  calling: string[],
): number => {
  let winningScore = -1;
  const remainingBoards = boards;

  // Call the numbers!
  iterateCalling: for (let i = 0; i < calling.length; i++) {
    // Ask each board if they won.
    for (let j = 0; j < remainingBoards.length; j++) {
      // If the last board, stop removing boards.
      if (remainingBoards.length === 1) {
        break;
      }
      const board = remainingBoards[j];
      const boardScore = checkBoardWin(board, calling.slice(0, i + 1));

      // If the board won, remove 'em and keep playing.
      if (boardScore > 0) {
        remainingBoards.splice(j, 1);
        j = 0;
      }
    }

    // If one board remains, keep playing until it wins.
    if (remainingBoards.length === 1) {
      winningScore = checkBoardWin(remainingBoards[0], calling.slice(0, i + 1));
      if (winningScore > 0) {
        break iterateCalling;
      }
    }
  }

  return winningScore;
};
