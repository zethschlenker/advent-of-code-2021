import input from './input.txt';
import { parseBoardInput, playGame, playLosingGame } from '@/days/04/helpers';
const parsedInput = parseBoardInput(input);

const { numbers, boards } = parsedInput;

const part1Result = playGame(boards, numbers);
const part2Result = playLosingGame(boards, numbers);

const Day: React.FC = () => {
  return (
    <>
      <h2>Day 04</h2>
      <p>Part 1: {part1Result}</p>
      <p>Part 2: {part2Result}</p>
    </>
  );
};

export default Day;
